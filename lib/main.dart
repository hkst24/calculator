import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';

void main() {
  runApp(const CalculatorApp());
}

class CalculatorApp extends StatelessWidget {
  const CalculatorApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const CupertinoApp(
      title: 'Калькулятор',
      home: CalculatorPage(),
    );
  }
}

class CalculatorPage extends StatefulWidget {
  const CalculatorPage({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _CalculatorPageState createState() => _CalculatorPageState();
}

class _CalculatorPageState extends State<CalculatorPage> {
  String _displayValue = '';

  void _handleButtonClick(String value) {
    setState(() {
      if (value == '=') { 
        try {
          _displayValue = evalExpression(_displayValue.replaceAll('×', ' * ').replaceAll('÷', ' / '));
        } catch (e) {
          _displayValue = _displayValue;
        }
      } else if (value == 'AC') {
        _displayValue = '';
      } else if (value == '%'){
        _handlePercentage();
      } else if (value == 'C') {
        _displayValue = _displayValue.replaceRange(_displayValue.length - 1, _displayValue.length, "");;
      } else if(_isValidInput(value)) {
          _displayValue += value;
      }
    });
  }

void _handlePercentage() {
  if (_displayValue.isNotEmpty) {
    final expression = _displayValue.replaceAll('×', '*').replaceAll('÷', '/');
    final percentage = double.parse(expression) / 100;
    _displayValue = percentage.toString();
  }
}

bool _isValidInput(String value) {
  final validCharacters = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '+', '-', '×', '÷', '%', '^'];

  if (_displayValue.isNotEmpty &&
      ['+', '-', '×', '÷', '%', '^'].contains(value) &&
      ['+', '-', '×', '÷', '%', '^'].contains(_displayValue[_displayValue.length - 1])) {
    _displayValue = _displayValue.replaceRange(_displayValue.length - 1, _displayValue.length, value);
    _displayValue = _displayValue.substring(0, _displayValue.length - 1);
    return true;
  }

  if (value == '.' && _displayValue.isNotEmpty && _displayValue.split(RegExp(r'[÷+-×%^]')).last.contains('.')) {
    return false;
  }

  if (_displayValue.isEmpty && ['+', '×', '÷', '%', '^'].contains(value) &&  value != '-') {
    return false;
  }

  if (_displayValue.isEmpty && (value == '.' || ['+', '×', '÷', '%', '^'].contains(value))) {
    return false;
  }

  if (value == '.' && _displayValue.contains('.')) {
    return false;
  }

  if (_displayValue.isNotEmpty &&
      (['+', '-', '×', '÷', '.', '%', '^'].contains(_displayValue[_displayValue.length - 1])) &&
      !['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-'].contains(value)) {
    return false;
  }

  if (value == '%' && _displayValue.isNotEmpty &&
      RegExp(r'\d').hasMatch(_displayValue[_displayValue.length - 1])) {
    return false;
  }

  return validCharacters.contains(value);
}

  String evalExpression(String expression) {

    Expression exp = Parser().parse(expression);
    ContextModel cm = ContextModel();
    double result = exp.evaluate(EvaluationType.REAL, cm);

    return result.toString();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      backgroundColor: CupertinoColors.black,
      navigationBar: const CupertinoNavigationBar(
        middle: Text('Калькулятор'),
        backgroundColor: CupertinoColors.black,
        border: null,
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Container(
                alignment: Alignment.bottomRight,
                child: Text(
                  _displayValue,
                  style: const TextStyle(
                    fontSize: 48.0,
                    fontWeight: FontWeight.bold,
                    color: CupertinoColors.white,
                  ),
                ),
              ),
            ),
            const SizedBox(height: 8.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _buildButton('AC', backgroundColor: CupertinoColors.systemGrey, textcolor: Colors.black),
                _buildButton('C', backgroundColor: CupertinoColors.systemGrey, textcolor: Colors.black),
                _buildButton('%', backgroundColor: CupertinoColors.systemGrey, textcolor: Colors.black),
                _buildButton('÷', backgroundColor: CupertinoColors.systemOrange),
              ],
            ),
            const SizedBox(height: 8.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _buildButton('7'),
                _buildButton('8'),
                _buildButton('9'),
                _buildButton('×', backgroundColor: CupertinoColors.systemOrange),
              ],
            ),
            const SizedBox(height: 8.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _buildButton('4'),
                _buildButton('5'),
                _buildButton('6'),
                _buildButton('-', backgroundColor: CupertinoColors.systemOrange),
              ],
            ),
            const SizedBox(height: 8.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _buildButton('1'),
                _buildButton('2'),
                _buildButton('3'),
                _buildButton('+', backgroundColor: CupertinoColors.systemOrange),
              ],
            ),
            const SizedBox(height: 8.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _buildButton('.'),
                _buildButton('0'),
                _buildButton('^'),
                _buildButton('=', backgroundColor: CupertinoColors.systemOrange),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildButton(String text, {Color backgroundColor = const Color.fromARGB(255, 49, 49, 49), int flex = 1, Color textcolor = const Color.fromARGB(255, 243, 243, 243)}) {
    return Expanded(
      flex: flex,
      child: RawMaterialButton(
        onPressed: () => _handleButtonClick(text),
        fillColor: backgroundColor,
        shape: const CircleBorder(),
        elevation: 0.0,
        padding: const EdgeInsets.all(20.0),
        child: Text(
          text,
          style: TextStyle(
            fontSize: 24.0,
            fontWeight: FontWeight.bold,
            color: textcolor,
          ),
        ),
      ),
    );
  }
}